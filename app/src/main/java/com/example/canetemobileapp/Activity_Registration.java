package com.example.canetemobileapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class Activity_Registration extends AppCompatActivity {
    EditText editTextBirthDate, editTextEmail, editTextFirstname, editTextLastname;
    Button buttonRegister;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);

        editTextFirstname = findViewById(R.id.editTextFirstname);
        editTextLastname = findViewById(R.id.editTextLastname);
        editTextBirthDate = findViewById(R.id.editTextBirthDate);
        editTextEmail = findViewById(R.id.editTextTextEmail);
        buttonRegister = findViewById(R.id.buttonRegister);

        buttonRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Activity_Registration.this, Activity_Display.class);
                intent.putExtra("fname_key", editTextFirstname.getText().toString());
                intent.putExtra("lname_key", editTextLastname.getText().toString());
                intent.putExtra("bdate_key", editTextBirthDate.getText().toString());
                intent.putExtra("bmail_key", editTextEmail.getText().toString());
                startActivity(intent);
            }
        });
    }
}