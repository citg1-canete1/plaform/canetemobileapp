package com.example.canetemobileapp;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

public class Activity_Display extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display);

        TextView textViewFirstname = findViewById(R.id.textViewFirstname);
        TextView textViewBirthdate = findViewById(R.id.textViewBirthdate);
        TextView textViewEmail = findViewById(R.id.textViewEmail);

        Intent intent = getIntent();

        String bdate = intent.getStringExtra("bdate_key");
        String bmail = intent.getStringExtra("bmail_key");

        textViewBirthdate.setText(bdate);
        textViewEmail.setText(bmail);
    }

}