package com.example.canetemobileapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    String msg = "CANETE Android : ";

    /** Called when the activity is first created. */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.d(msg, "The onCreate() event");

        Button button2 = findViewById(R.id.btnActivity2);
        button2.setOnClickListener(this);

        Button buttonSendSMS = findViewById(R.id.btnSendSMS);
        buttonSendSMS.setOnClickListener(this);

        Button buttonOpenMap = findViewById(R.id.btnOpenMap);
        buttonOpenMap.setOnClickListener(this);



    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnActivity2:
                Toast.makeText(MainActivity.this, "Activity 2 button is clicked!", Toast.LENGTH_SHORT).show();
                Intent intent2 = new Intent(MainActivity.this, MainActivity2_Menu.class);
                startActivity(intent2);
                break;
            case R.id.btnSendSMS:
                Toast.makeText(MainActivity.this, "Send SMS button is clicked!", Toast.LENGTH_SHORT).show();
                Intent intent3 = new Intent(MainActivity.this, Activity_SendSMSPage.class);
                startActivity(intent3);
                break;

            case R.id.btnOpenMap:
                Intent intent4 = new Intent(Intent.ACTION_VIEW, Uri.parse("geo:10.316720, 123.890710"));
                startActivity(intent4);
                break;

        }
    }

    /** Called when the activity is about to become visible. */
    @Override
    protected void  onStart(){
        super.onStart();
        Log.d(msg, "The onStart() event");
    }

    /** Called when another activity is taking focus. */
    @Override
    protected void  onPause(){
        super.onPause();
        Log.d(msg, "The onPause() event");
    }

    /** Called when the activity is no longer visible. */
    @Override
    protected void  onStop(){
        super.onStop();
        Log.d(msg, "The onStop() event");
    }

    /** Called just before the activity is destroyed. */
    @Override
    protected void  onDestroy(){
        super.onDestroy();
        Log.d(msg, "The onDestroy() event");
    }

    private class MyClickListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            // Do something when the button is clicked
            Toast.makeText(MainActivity.this, "Button clicked!", Toast.LENGTH_SHORT).show();
        }
    }
}